<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-tables', function(){
    return view('table.data-tables');
});

//CRUD Cast

//menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', 'CastController@create');

//menyimpan data baru ke tabel Cast
Route::post('/cast', 'CastController@store');

//menampilkan list data para pemain film 
Route::get('/cast', 'CastController@index');

//menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', 'CastController@show');

//menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', 'CastController@update');

//menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', 'CastController@destroy');






